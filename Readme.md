# Spring Boot Task Scheduling Demo

## Requirements

1. Java - 1.8.x
2. Maven - 3.x.x

## Steps to setup

1. cd spring-boot-scheduler-example

2. Run mvn clean install -DskipTests=true

3. Copy the war file from target and move to tomcat webapps

4. Place any sample xls file under /tmp/data/excel/processing

5. Start tomcat server

6. Check logs. You can see the cells in xls are printed in console. Once done, its moved to processed folder.

# Easy setup for SOAP WS

1. cd spring-boot-scheduler-example

2. Run mvn clean spring-boot:run

3. This will internally start Tomcat at 8080. This is best rather than building as war and deploying in tomcat.

Testing :

1. Install Wizdler chrome plugin. After restarting chrome, hit http://localhost:8080/ws/messageWsdl.wsdl to see the wsdl

2. Click wizdler icon and click sendMessage link




