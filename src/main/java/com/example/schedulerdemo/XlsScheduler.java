package com.example.schedulerdemo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Karthick on 14/8/18.
 */

@Component
public class XlsScheduler {

	private static final Logger logger = LoggerFactory.getLogger(XlsScheduler.class);

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	private ExcelFileConsoleReader excelFileConsoleReader;

	@Autowired
	public XlsScheduler(ExcelFileConsoleReader excelFileConsoleReader) {
		this.excelFileConsoleReader = excelFileConsoleReader;
	}

	/**
	 * Scheduler method which executes for fixedRate interval
	 *
	 */

	@Scheduled(fixedRate = 5000, initialDelay = 10000)
	public void scheduleTaskWithFixedRate() {
		logger.info("Checking for files ::", dateTimeFormatter.format(LocalDateTime.now()));
		excelFileConsoleReader.process();
	}


}
