package com.example.schedulerdemo;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by Karthick on 14/8/18.
 */

@Service
public class ExcelFileConsoleReader extends AbstractExcelFileReader {

	private static final Logger logger = LoggerFactory.getLogger(ExcelFileConsoleReader.class);
	
	private static final String PROCESSING_FILE_LOCATION = "/tmp/data/excel/processing";
	private static final String PROCESSED_FILE_LOCATION = "/tmp/data/excel/processed";
	private static final String ERROR_FILE_LOCATION = "/tmp/data/excel/error";

	/**
	 * Scheduler executes this method for every specified time interval
	 */
	public void process() {
		List<File> files = findAllExcelFiles(PROCESSING_FILE_LOCATION);
		if (files.size() == 0) {
			logger.info("There is no file found for processing.");
		} else {
			logger.info("Excel file console reader started with file size:: " + files.size());
		}


		files.stream().forEachOrdered(file -> {
			logger.info("Form processing :: " + file.getName());
			boolean errorOccured = Boolean.FALSE;
			// Process each file and throw it to console output
			try (XSSFWorkbook xssfWorkbook = retrieveWorkbook(file)) {
				consoleContentPrinter(xssfWorkbook.getSheetAt(0));
			} catch (Exception e) {
				logger.error("Error in processing file", e);
				errorOccured = Boolean.TRUE;
			} finally {
				moveFile(errorOccured ? ERROR_FILE_LOCATION : PROCESSED_FILE_LOCATION, file);
			}
		});

	}

	/**
	 * Parse and print the sheet data to console
	 */
	private void consoleContentPrinter(XSSFSheet sheetAt) {
		Iterator<Row> iterator = sheetAt.iterator();
		while (iterator.hasNext()) {
			Row currentRow = iterator.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			while (cellIterator.hasNext()) {
				Cell currentCell = cellIterator.next();
				// getCellTypeEnum shown as deprecated for version 3.15
				// getCellTypeEnum ill be renamed to getCellType starting from version 4.0
				if (currentCell.getCellTypeEnum() == CellType.STRING) {
					logger.info(currentCell.getStringCellValue() + "--");
				} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
					logger.info(currentCell.getNumericCellValue() + "--");
				}
			}
		}
	}

}
