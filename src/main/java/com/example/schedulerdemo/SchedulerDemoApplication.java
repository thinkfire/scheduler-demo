package com.example.schedulerdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Springs main class to run the application
 *
 */

@SpringBootApplication
@EnableScheduling
public class SchedulerDemoApplication  extends SpringBootServletInitializer{
	public static void main(String[] args) {
		SpringApplication.run(SchedulerDemoApplication.class, args);
	}
}


