package com.example.schedulerdemo.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

/**
 * Created by Karthick on 17/8/18.
 */

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    /**
     * Set dispatcher servlet for spring web service
     *
     * @param applicationContext
     * @return ServletRegistrationBean
     */

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }

    /**
     * This bean determines the URL where WSDL is available. In this case
     * it will be available at http://<host>:<port>/ws/messageWsdl.wsdl
     *
     * @param messageSchema
     * @return wsdl11Definition
     */

    @Bean(name = "messageWsdl")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema messageSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MessagesPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://generated.message.webservice.com");
        wsdl11Definition.setSchema(messageSchema);
        return wsdl11Definition;
    }

    /**
     * Returns xsd schema under resources file
     *
     * @return xsdSchema
     */
    @Bean
    public XsdSchema messageSchema() {
        return new SimpleXsdSchema(new ClassPathResource("sendmessage.xsd"));
    }
}
