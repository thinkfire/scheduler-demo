package com.example.schedulerdemo.config;

import com.webservice.message.generated.SendMesssageRequest;
import com.webservice.message.generated.SendMesssageResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 *
 * Endpoint registers this class with Spring WS
 * Created by Karthick on 17/8/18.
 */
@Endpoint
public class MessageEndPoint {

    private static final String NAMESPACE_URI = "http://generated.message.webservice.com";

    /**
     * PayloadRoot is used by spring WS to pick the method based on namespace and localpart
     *
     * @param request
     * @return sendMesssageResponse
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SendMesssageRequest")
    @ResponsePayload
    public SendMesssageResponse sendMesssageRequest(@RequestPayload SendMesssageRequest request) {
        SendMesssageResponse sendMesssageResponse = new SendMesssageResponse();
        sendMesssageResponse.setStatus("Message sent from "+request.getUser());
        return sendMesssageResponse;
    }
}
