package com.example.schedulerdemo;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Karthick on 14/8/18.
 */
public abstract class AbstractExcelFileReader {

	private static final Logger logger = LoggerFactory.getLogger(AbstractExcelFileReader.class);

	/**
	 * ExcelFileFilter Class to read .xls file
	 */
	public class ExcelFileFilter implements FileFilter {
		@Override
		public boolean accept(File file) {
			return file != null && file.isFile() && file.canRead()
					&& (file.getName().endsWith("xls") || file.getName().endsWith("xlsx"));
		}
	}

	/**
	 * Find all .xls files for the input location and return
	 *
	 * @param dirlocation
	 * @return listFiles
	 */

	protected List<File> findAllExcelFiles(String dirlocation) {
		File folder = new File(dirlocation);
		if (!folder.exists()) {
			folder.mkdir();
		}
		File[] listFiles = folder.listFiles(new ExcelFileFilter());
		if (Objects.isNull(listFiles)) {
			return Arrays.asList();
		} else {
			return Arrays.asList(listFiles);
		}
	}

	/**
	 * Retrieve particular workbook for a give .xls file
	 *
	 * @param file
	 * @return XSSFWorkbook
	 */

	protected XSSFWorkbook retrieveWorkbook(File file) throws IOException {
		FileInputStream excelFile = new FileInputStream(file);
		return new XSSFWorkbook(excelFile);
	}

	/**
	 * Move file to to processed location is success, else to error location
	 *
	 * @param location
	 * @param file
	 */
	protected void moveFile(String location, File file) {
		File locationFile = new File(location);
		if (!locationFile.exists()) {
			locationFile.mkdir();
		}
		if (file.renameTo(new File(location + "/" + file.getName()))) {
			logger.info("File is moved successful!");
		} else {
			logger.info("File is failed to move!");
		}
	}
}
